# qtrsensortest

This is the Sample Code by Pololu adjusted for 16 instead of 6 analog sensors.

Connected to an Arduino Mega 2560 like this:

* EVN -> D2
* ODD -> D3
* S1..S16 -> A0..A15
* VCC -> 5V
* GND -> GND
